package main

import (
	"credit-control-kent/app"
	"credit-control-kent/config"
)

func main() {
	config := config.GetConfig()
	app := &app.App{}
	app.Initialize(config)
	app.Run(":8095")

}
