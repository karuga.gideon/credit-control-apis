package handler

import (
	"encoding/json"
	"log"
	"net/http"
	"strconv"
	"time"

	"credit-control-kent/app/model"

	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
)

func GetAllClients(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	clientsReport := model.ClientsApiReport{}
	clients := []model.Client{}
	db.Order("Id DESC").Find(&clients)
	clientsReport.ClientsCount = CountClients(db)
	clientsReport.Clients = clients
	respondJSON(w, http.StatusOK, clientsReport)
}

func CreateClient(db *gorm.DB, w http.ResponseWriter, r *http.Request) {

	user := model.Client{}
	var last_user model.Client
	db.Last(&last_user)
	user.ID = last_user.ID + 1
	user.Status = 1
	user.DateCreated = time.Now()

	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&user); err != nil {
		respondError(w, http.StatusBadRequest, err.Error())
		return
	}
	defer r.Body.Close()

	if err := db.Save(&user).Error; err != nil {
		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}

	activity := model.Activity{}
	activity.UserID = user.ID
	activity.ActivityType = "CLIENT_CREATION"
	activity.Description = "Client created successfully."
	CreateActivity(db, activity)

	respondJSON(w, http.StatusCreated, user)
}

func GetClient(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	user_id := vars["id"]
	user := getClientOr404(db, user_id, w, r)
	if user == nil {
		return
	}
	respondJSON(w, http.StatusOK, user)
}

func UpdateClient(db *gorm.DB, w http.ResponseWriter, r *http.Request) {

	vars := mux.Vars(r)
	user_id := vars["id"]
	user := getClientOr404(db, user_id, w, r)
	if user == nil {
		return
	}

	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&user); err != nil {
		respondError(w, http.StatusBadRequest, err.Error())
		return
	}
	defer r.Body.Close()

	if err := db.Save(&user).Error; err != nil {
		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}
	respondJSON(w, http.StatusOK, user)
}

func DeleteClient(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	user_id := vars["id"]
	user := getClientOr404(db, user_id, w, r)
	if user == nil {
		return
	}
	if err := db.Delete(&user).Error; err != nil {
		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}
	respondJSON(w, http.StatusNoContent, nil)
}

func ArchiveClient(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	user_id := vars["id"]
	user := getClientOr404(db, user_id, w, r)
	if user == nil {
		return
	}
	if err := db.Save(&user).Error; err != nil {
		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}
	respondJSON(w, http.StatusOK, user)
}

func RestoreClient(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	user_id := vars["id"]
	user := getClientOr404(db, user_id, w, r)
	if user == nil {
		return
	}
	if err := db.Save(&user).Error; err != nil {
		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}
	respondJSON(w, http.StatusOK, user)
}

func CountClients(db *gorm.DB) float64 {
	var count float64
	db.Table("clients").Count(&count)
	return count
}

// GetClientDetails - GetClientDetails
func GetClientDetails(db *gorm.DB, clientID int) *model.Client {
	client := model.Client{}
	if err := db.First(&client, model.Client{ID: clientID}).Error; err != nil {
		return &client
	}
	return &client
}

// getClientOr404 gets a user instance if exists, or respond the 404 error otherwise
func getClientOr404(db *gorm.DB, user_id string, w http.ResponseWriter, r *http.Request) *model.Client {
	user := model.Client{}
	n, err := strconv.Atoi(user_id)
	if err != nil {
		log.Println("Error converting user_id to int >>> ", err)
	}

	if err := db.First(&user, model.Client{ID: n}).Error; err != nil {
		respondError(w, http.StatusNotFound, err.Error())
		return nil
	}
	return &user
}
