package handler

import (
	"encoding/json"
	"log"
	"net/http"
	"strconv"
	"time"

	"credit-control-kent/app/model"

	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
)

func GetAllSalesRepresentatives(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	apiResponse := model.SalesRepsApiReport{}
	salesReps := []model.SalesRepresentative{}
	db.Order("Id DESC").Find(&salesReps)
	apiResponse.SalesRepsCount = CountSalesRepresentatives(db)
	apiResponse.SalesRepresentatives = salesReps
	respondJSON(w, http.StatusOK, apiResponse)
}

func CreateSalesRepresentative(db *gorm.DB, w http.ResponseWriter, r *http.Request) {

	user := model.SalesRepresentative{}
	var last_user model.SalesRepresentative
	db.Last(&last_user)
	user.ID = last_user.ID + 1
	user.DateCreated = time.Now()

	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&user); err != nil {
		respondError(w, http.StatusBadRequest, err.Error())
		return
	}
	defer r.Body.Close()

	if err := db.Save(&user).Error; err != nil {
		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}
	respondJSON(w, http.StatusCreated, user)
}

func GetSalesRepresentative(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	user_id := vars["id"]
	user := getSalesRepresentativeOr404(db, user_id, w, r)
	if user == nil {
		return
	}
	respondJSON(w, http.StatusOK, user)
}

func UpdateSalesRepresentative(db *gorm.DB, w http.ResponseWriter, r *http.Request) {

	vars := mux.Vars(r)
	user_id := vars["id"]
	user := getSalesRepresentativeOr404(db, user_id, w, r)
	if user == nil {
		return
	}

	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&user); err != nil {
		respondError(w, http.StatusBadRequest, err.Error())
		return
	}
	defer r.Body.Close()

	if err := db.Save(&user).Error; err != nil {
		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}
	respondJSON(w, http.StatusOK, user)
}

func DeleteSalesRepresentative(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	user_id := vars["id"]
	user := getSalesRepresentativeOr404(db, user_id, w, r)
	if user == nil {
		return
	}
	if err := db.Delete(&user).Error; err != nil {
		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}
	respondJSON(w, http.StatusNoContent, nil)
}

func ArchiveSalesRepresentative(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	user_id := vars["id"]
	user := getSalesRepresentativeOr404(db, user_id, w, r)
	if user == nil {
		return
	}
	if err := db.Save(&user).Error; err != nil {
		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}
	respondJSON(w, http.StatusOK, user)
}

func RestoreSalesRepresentative(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	user_id := vars["id"]
	user := getSalesRepresentativeOr404(db, user_id, w, r)
	if user == nil {
		return
	}
	if err := db.Save(&user).Error; err != nil {
		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}
	respondJSON(w, http.StatusOK, user)
}

// Count SalesRepresentatives
func CountSalesRepresentatives(db *gorm.DB) float64 {
	var count float64
	db.Table("sales_representatives").Count(&count)
	return count
}

// GetSalesRepresentativeDetails - GetSalesRepresentativeDetails
func GetSalesRepresentativeDetails(db *gorm.DB, salesRepresentativeID int) *model.SalesRepresentative {
	salesRepresentative := model.SalesRepresentative{}
	if err := db.First(&salesRepresentative, model.SalesRepresentative{ID: salesRepresentativeID}).Error; err != nil {
		return &salesRepresentative
	}
	return &salesRepresentative
}

// getSalesRepresentativeOr404 gets a user instance if exists, or respond the 404 error otherwise
func getSalesRepresentativeOr404(db *gorm.DB, user_id string, w http.ResponseWriter, r *http.Request) *model.SalesRepresentative {
	user := model.SalesRepresentative{}
	n, err := strconv.Atoi(user_id)
	if err != nil {
		log.Println("Error converting user_id to int >>> ", err)
	}

	if err := db.First(&user, model.SalesRepresentative{ID: n}).Error; err != nil {
		respondError(w, http.StatusNotFound, err.Error())
		return nil
	}
	return &user
}
