package handler

import (
	"credit-control-kent/app/model"
	"credit-control-kent/app/utils"
	"credit-control-kent/config"
	"log"
	"net/http"
	"strings"
	"time"

	"github.com/jinzhu/gorm"
)

var key = "EuXqfey1AeDwo9S3jk6JM5ai9OZO2yW3ejQHOqb5Da8jKwdV6BiWpo9uBgiujBCL9p2F01Hko9bY258BBZsfrtO6vSt2zke4zBN5"

// HealthCheck - HealthCheck
// swagger:route GET /status HealthCheck HealthCheck
// Returns the status of the service, whether it's up or not.
// JSON Response is provided with respective success or fail details.
// responses:
//  200: jsonResponse
//  403: forbidddenResponse
func HealthCheck(config *config.Config, db *gorm.DB, w http.ResponseWriter, r *http.Request) {

	log.Println("Checking status....")

	// signature := generateSignature()
	// ValidateSignature(signature)

	license := config.Keys.License
	isLicensed := ValidateSignature(license)
	// log.Println("isLicensed   >>> ", isLicensed)

	response := model.JSONResponse{}

	if isLicensed {
		response := model.JSONResponse{}
		response.Status = "00"
		response.Description = "OK. All services up."
		respondJSON(w, http.StatusOK, response)
	} else {
		response.Status = "01"
		response.Description = "Licenese Expired! Please contact your system provider."
		respondJSON(w, 403, response)
	}

}

// generateSignature - generateSignature
func generateSignature() string {
	log.Println("Generating signature...")
	signature := "2019-05-28 : HSC 2019 License by Struts Technology."
	encrypted, err1 := utils.EncryptString(signature, key)
	utils.CheckError(err1, "Error encrypting string.")
	log.Println("encrypted  >>> ", encrypted)
	return encrypted
}

// ValidateSignature - ValidateSignature
func ValidateSignature(encrypted string) bool {
	// log.Println("Validating license...")
	decrypted, err2 := utils.DecryptString(encrypted, key)
	utils.CheckError(err2, "Error decrypting string.")
	// log.Println("decrypted  >>> ", decrypted)

	// 29-03-2019 : HSC 2019 License by Struts Technology.
	signatureArray := strings.Split(decrypted, ":")
	date := signatureArray[0]
	comment := signatureArray[1]

	date = strings.Replace(date, " ", "", -1)
	comment = strings.Replace(comment, " ", "", -1)

	// log.Println("Epiry Date  >>> ", date)
	// log.Println("Comment     >>> ", comment)

	expiryDate, err3 := time.Parse("2006-01-02", date)
	utils.CheckError(err3, "Error parsing expiry date!")
	// log.Println("Epiry Date  >>> ", expiryDate)

	// Check if within validity period
	if expiryDate.After(time.Now()) {
		// log.Println("License is Valid.")
		return true
	} else {
		log.Println("License is Expired!")
		return false
	}

}
