package handler

import (
	"credit-control-kent/app/model"
	"net/http"

	"github.com/jinzhu/gorm"
)

func GetSummaryReport(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	summaryReport := model.SummaryReport{}
	summaryReport.CollectionsCount = CountCollections(db)
	summaryReport.ClientsCount = CountClients(db)
	summaryReport.SalesRepresentativesCount = CountSalesRepresentatives(db)
	summaryReport.UsersCount = CountUsers(db)
	respondJSON(w, http.StatusOK, summaryReport)
}
