package handler

import (
	"encoding/json"
	"log"
	"net/http"
	"strconv"
	"time"

	"credit-control-kent/app/model"
	"credit-control-kent/app/utils"

	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
)

func GetAllCollections(db *gorm.DB, w http.ResponseWriter, r *http.Request) {

	collections := []model.Collection{}
	db.Order("Id DESC").Find(&collections)

	collectionsReport := []model.CollectionsReport{}
	for _, collection := range collections {
		collectionReport := model.CollectionsReport{}
		collectionReport.Collection = collection
		collectionReport.Client = *GetClientDetails(db, collection.ClientID)
		collectionReport.SalesRepresentative = *GetSalesRepresentativeDetails(db, collection.SalesRepresentativeID)
		collectionsReport = append(collectionsReport, collectionReport)
	}

	apiReport := model.CollectionsApiReport{}
	apiReport.CollectionsCount = CountCollections(db)
	apiReport.Collections = collectionsReport

	respondJSON(w, http.StatusOK, apiReport)
}

func CreateCollection(db *gorm.DB, w http.ResponseWriter, r *http.Request) {

	user := model.Collection{}
	var last_user model.Collection
	db.Last(&last_user)
	user.ID = last_user.ID + 1
	user.DateCreated = time.Now()

	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&user); err != nil {
		respondError(w, http.StatusBadRequest, err.Error())
		return
	}
	defer r.Body.Close()

	if err := db.Save(&user).Error; err != nil {
		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}

	activity := model.Activity{}
	activity.UserID = user.ID
	activity.ActivityType = "COLLECTION_CREATION"
	activity.Description = "Collection created successfully."
	CreateActivity(db, activity)

	respondJSON(w, http.StatusCreated, user)
}

func GetCollection(db *gorm.DB, w http.ResponseWriter, r *http.Request) {

	vars := mux.Vars(r)
	collectionIDString := vars["id"]
	collectionID := utils.ConvertStringToInt(collectionIDString)

	collectionReport := model.CollectionsReport{}
	collection := GetCollectionDetails(db, collectionID)
	collectionReport.Collection = *collection
	collectionReport.Client = *GetClientDetails(db, collection.ClientID)
	collectionReport.SalesRepresentative = *GetSalesRepresentativeDetails(db, collection.SalesRepresentativeID)

	respondJSON(w, http.StatusOK, collectionReport)
}

func UpdateCollection(db *gorm.DB, w http.ResponseWriter, r *http.Request) {

	vars := mux.Vars(r)
	collectionIDString := vars["id"]
	collectionID := utils.ConvertStringToInt(collectionIDString)
	collection := GetCollectionDetails(db, collectionID)

	collectionUpdate := model.Collection{}
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&collectionUpdate); err != nil {
		respondError(w, http.StatusBadRequest, err.Error())
		return
	}
	defer r.Body.Close()

	collection.InvoiceNumber = collectionUpdate.InvoiceNumber
	collection.InvoiceDate = collectionUpdate.InvoiceDate
	collection.DueDate = collectionUpdate.DueDate
	collection.CreditPeriod = collectionUpdate.CreditPeriod
	collection.Amount = collectionUpdate.Amount
	collection.AdvancePayment = collectionUpdate.AdvancePayment
	collection.Balance = collectionUpdate.Balance
	collection.SalesCommission = collectionUpdate.SalesCommission
	collection.ClientID = collectionUpdate.ClientID
	collection.SalesRepresentativeID = collectionUpdate.SalesRepresentativeID

	if err := db.Save(&collection).Error; err != nil {
		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}
	respondJSON(w, http.StatusOK, collection)
}

func DeleteCollection(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	user_id := vars["id"]
	user := getCollectionOr404(db, user_id, w, r)
	if user == nil {
		return
	}
	if err := db.Delete(&user).Error; err != nil {
		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}
	respondJSON(w, http.StatusNoContent, nil)
}

func ArchiveCollection(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	user_id := vars["id"]
	user := getCollectionOr404(db, user_id, w, r)
	if user == nil {
		return
	}
	if err := db.Save(&user).Error; err != nil {
		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}
	respondJSON(w, http.StatusOK, user)
}

func RestoreCollection(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	user_id := vars["id"]
	user := getCollectionOr404(db, user_id, w, r)
	if user == nil {
		return
	}
	if err := db.Save(&user).Error; err != nil {
		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}
	respondJSON(w, http.StatusOK, user)
}

func CountCollections(db *gorm.DB) float64 {
	var count float64
	db.Table("collections").Count(&count)
	return count
}

// GetCollectionDetails - GetCollectionDetails
func GetCollectionDetails(db *gorm.DB, collectionID int) *model.Collection {
	collection := model.Collection{}
	if err := db.First(&collection, model.Collection{ID: collectionID}).Error; err != nil {
		return &collection
	}
	return &collection
}

// getCollectionOr404 gets a user instance if exists, or respond the 404 error otherwise
func getCollectionOr404(db *gorm.DB, user_id string, w http.ResponseWriter, r *http.Request) *model.Collection {
	user := model.Collection{}
	n, err := strconv.Atoi(user_id)
	if err != nil {
		log.Println("Error converting user_id to int >>> ", err)
	}

	if err := db.First(&user, model.Collection{ID: n}).Error; err != nil {
		respondError(w, http.StatusNotFound, err.Error())
		return nil
	}
	return &user
}
