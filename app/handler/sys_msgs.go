package handler

import (
	"credit-control-kent/app/model"
	"net/http"
	"time"

	"github.com/jinzhu/gorm"
)

func CreateSysMsg(db *gorm.DB, newSysMsg model.SysMsg) {

	sysMsg := model.SysMsg{}
	var lastSysMsg model.SysMsg
	db.Last(&lastSysMsg)
	sysMsg.ID = lastSysMsg.ID + 1
	sysMsg.DateCreated = time.Now()
	sysMsg.MessageType = newSysMsg.MessageType
	sysMsg.Recipient = newSysMsg.Recipient
	sysMsg.Subject = newSysMsg.Subject
	sysMsg.Message = newSysMsg.Message

	db.Save(&sysMsg)
}

func GetAllSysMsgs(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	sysMsg := []model.SysMsg{}
	db.Order("Id DESC").Find(&sysMsg)
	respondJSON(w, http.StatusOK, sysMsg)
}
