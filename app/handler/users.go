package handler

import (
	"encoding/json"
	"log"
	"net/http"
	"strconv"
	"strings"
	"time"

	"credit-control-kent/app/model"
	"credit-control-kent/app/utils"

	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
)

// passphrase for passwords encryption
var passphrase = "DgetereFFGGfegegeFd343gtKwDMONwYuZMRyEwOMuzXNSrGpuzvdvUrgW25656"

// Authenticate user before accessing API
func Authenticate(db *gorm.DB, w http.ResponseWriter, r *http.Request) {

	w.Header().Set("Content-Type", "application/json")
	basicAuth := r.Header.Get("Authorization")
	s := strings.Split(basicAuth, " ")
	value := s[1]
	details := utils.Base64DecodeString(value)
	userDetails := strings.Split(details, ":")
	email, password := userDetails[0], userDetails[1]

	// For tests - initial user creation
	// encryptedPwd, _ := utils.EncryptString(password, passphrase)
	// log.Println("encryptedPwd   >>>> ", encryptedPwd)

	userValidation := model.User{}
	db.Where("email = ? or username = ? ", email, email).Find(&userValidation).First(&userValidation)

	if userValidation.ID != 0 {

		activity := model.Activity{}
		activity.UserID = userValidation.ID
		activity.ActivityType = "USER_LOGIN"

		decrypted, err := utils.DecryptString(userValidation.Password, passphrase)
		if err != nil {
			log.Println(err)
		}
		if password == decrypted {
			activity.Description = "Login Successful."
			utils.CreateTokenEndpointCustom(w, userValidation)
		} else {
			activity.Description = "Login Failed!"
			w.WriteHeader(http.StatusUnauthorized)
		}

		CreateActivity(db, activity)

	} else {
		w.WriteHeader(http.StatusUnauthorized)
	}
}

func GetAllUsers(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	apiResponse := model.UsersRepsApiReport{}
	users := []model.User{}
	db.Order("Id DESC").Find(&users)
	apiResponse.UsersCount = CountUsers(db)
	apiResponse.Users = users
	respondJSON(w, http.StatusOK, apiResponse)
}

func CreateUser(db *gorm.DB, w http.ResponseWriter, r *http.Request) {

	user := model.User{}
	var last_user model.User
	db.Last(&last_user)
	user.ID = last_user.ID + 1
	user.Status = "ACTIVE"
	user.DateCreated = time.Now()

	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&user); err != nil {
		respondError(w, http.StatusBadRequest, err.Error())
		return
	}
	defer r.Body.Close()

	encrypted, err := utils.EncryptString(user.Password, passphrase)
	if err != nil {
		log.Println(err)
	} else {
		user.Password = encrypted
	}

	if err := db.Save(&user).Error; err != nil {
		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}

	activity := model.Activity{}
	activity.UserID = user.ID
	activity.ActivityType = "USER_CREATION"
	activity.Description = "User created successfully."
	CreateActivity(db, activity)

	respondJSON(w, http.StatusCreated, user)
}

func GetUser(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	user_id := vars["id"]
	user := getUserOr404(db, user_id, w, r)
	if user == nil {
		return
	}
	respondJSON(w, http.StatusOK, user)
}

func UpdateUser(db *gorm.DB, w http.ResponseWriter, r *http.Request) {

	vars := mux.Vars(r)
	user_id := vars["id"]
	user := getUserOr404(db, user_id, w, r)
	if user == nil {
		return
	}

	updateUserDetails := model.User{}
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&updateUserDetails); err != nil {
		respondError(w, http.StatusBadRequest, err.Error())
		return
	}
	defer r.Body.Close()

	user.Name = updateUserDetails.Name
	user.Phone = updateUserDetails.Phone
	user.Email = updateUserDetails.Email
	user.Username = updateUserDetails.Username

	if updateUserDetails.Password != "" {
		// Encrypt the users pwd before saving to db
		encrypted, err := utils.EncryptString(updateUserDetails.Password, passphrase)
		if err != nil {
			log.Println(err)
		} else {
			user.Password = encrypted
		}
	}

	if err := db.Save(&user).Error; err != nil {
		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}
	respondJSON(w, http.StatusOK, user)
}

func DeleteUser(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	user_id := vars["id"]
	user := getUserOr404(db, user_id, w, r)
	if user == nil {
		return
	}
	if err := db.Delete(&user).Error; err != nil {
		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}
	respondJSON(w, http.StatusNoContent, nil)
}

func ArchiveUser(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	user_id := vars["id"]
	user := getUserOr404(db, user_id, w, r)
	if user == nil {
		return
	}
	if err := db.Save(&user).Error; err != nil {
		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}
	respondJSON(w, http.StatusOK, user)
}

func RestoreUser(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	user_id := vars["id"]
	user := getUserOr404(db, user_id, w, r)
	if user == nil {
		return
	}
	if err := db.Save(&user).Error; err != nil {
		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}
	respondJSON(w, http.StatusOK, user)
}

// GeneratePassword
func GeneratePassword(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	pwd := utils.GenerateRandomString(40)
	respondJSON(w, http.StatusOK, pwd)
}

func CountUsers(db *gorm.DB) float64 {
	var count float64
	db.Table("users").Count(&count)
	return count
}

// GetUserDetails - GetUserDetails
func GetUserDetails(db *gorm.DB, userID int) *model.User {
	user := model.User{}
	if err := db.First(&user, model.User{ID: userID}).Error; err != nil {
		return &user
	}
	return &user
}

// getUserOr404 gets a user instance if exists, or respond the 404 error otherwise
func getUserOr404(db *gorm.DB, user_id string, w http.ResponseWriter, r *http.Request) *model.User {
	user := model.User{}
	n, err := strconv.Atoi(user_id)
	if err != nil {
		log.Println("Error converting user_id to int >>> ", err)
	}

	if err := db.First(&user, model.User{ID: n}).Error; err != nil {
		respondError(w, http.StatusNotFound, err.Error())
		return nil
	}
	return &user
}
