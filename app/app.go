package app

import (
	"fmt"
	"log"
	"net/http"

	"credit-control-kent/app/handler"
	"credit-control-kent/app/model"
	"credit-control-kent/app/utils"
	"credit-control-kent/config"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"

	"github.com/jinzhu/gorm"
	// For connecting to mysql db
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

// App has router and db instances
type App struct {
	Router *mux.Router
	DB     *gorm.DB
	Config *config.Config
}

// Initialize initializes the app with predefined configuration
func (a *App) Initialize(config config.Config) {

	dbURI := fmt.Sprintf("%s:%s@/%s?charset=utf8&parseTime=True&loc=Local",
		config.Database.Username,
		config.Database.Password,
		config.Database.DBName)

	db, err := gorm.Open(config.Database.Dialect, dbURI)
	if err != nil {
		log.Fatal("Could not connect to database")
	} else {
		log.Println("Connected to database.")
	}

	router := mux.NewRouter()

	// Web reporting portal
	webDir := "/web/"
	router.
		PathPrefix(webDir).
		Handler(http.StripPrefix(webDir, http.FileServer(http.Dir("."+webDir))))

	a.DB = model.DBMigrate(db)
	a.Router = router
	a.Config = &config

	// Check License validity
	license := config.Keys.License
	isLicensed := handler.ValidateSignature(license)

	if isLicensed {
		a.setRouters()
	} else {
		panic("License Expired!")
		// os.Exit(3)
	}

}

// Get wraps the router for GET method
func (a *App) Get(path string, f func(w http.ResponseWriter, r *http.Request)) {
	a.Router.HandleFunc(path, f).Methods("GET")
}

// Post wraps the router for POST method
func (a *App) Post(path string, f func(w http.ResponseWriter, r *http.Request)) {
	a.Router.HandleFunc(path, f).Methods("POST")
}

// Put wraps the router for PUT method
func (a *App) Put(path string, f func(w http.ResponseWriter, r *http.Request)) {
	a.Router.HandleFunc(path, f).Methods("PUT")
}

// Delete wraps the router for DELETE method
func (a *App) Delete(path string, f func(w http.ResponseWriter, r *http.Request)) {
	a.Router.HandleFunc(path, f).Methods("DELETE")
}

// setRouters sets the all required routers
func (a *App) setRouters() {

	appURLName := "/creditcontrol/"
	a.Get(appURLName+"authenticate", a.Authenticate)

	// Users routing
	a.Post(appURLName+"users", utils.ValidateMiddleware(a.CreateUser))
	a.Get(appURLName+"users", utils.ValidateMiddleware(a.GetAllUsers))
	a.Get(appURLName+"users/{id}", utils.ValidateMiddleware(a.GetUser))
	a.Put(appURLName+"users/{id}", utils.ValidateMiddleware(a.UpdateUser))
	a.Post(appURLName+"users/reset_pwd/{id}", utils.ValidateMiddleware(a.ResetUserPwd))
	a.Get(appURLName+"users/generate_pwd", utils.ValidateMiddleware(a.GeneratePassword))

	// Clients Routing
	a.Post(appURLName+"clients", utils.ValidateMiddleware(a.CreateClient))
	a.Get(appURLName+"clients", utils.ValidateMiddleware(a.GetAllClients))
	a.Get(appURLName+"clients/{id}", utils.ValidateMiddleware(a.GetClient))
	a.Put(appURLName+"clients/{id}", utils.ValidateMiddleware(a.UpdateClient))

	// Sales Representatives
	a.Post(appURLName+"salesrepresentatives", utils.ValidateMiddleware(a.CreateSalesRepresentative))
	a.Get(appURLName+"salesrepresentatives", utils.ValidateMiddleware(a.GetAllSalesRepresentatives))
	a.Get(appURLName+"salesrepresentatives/{id}", utils.ValidateMiddleware(a.GetSalesRepresentative))
	a.Put(appURLName+"salesrepresentatives/{id}", utils.ValidateMiddleware(a.UpdateSalesRepresentative))

	// Collections
	a.Post(appURLName+"collections", utils.ValidateMiddleware(a.CreateCollection))
	a.Get(appURLName+"collections", utils.ValidateMiddleware(a.GetAllCollections))
	a.Get(appURLName+"collections/{id}", utils.ValidateMiddleware(a.GetCollection))
	a.Put(appURLName+"collections/{id}", utils.ValidateMiddleware(a.UpdateCollection))

	// Reports
	a.Get(appURLName+"reports/summary", utils.ValidateMiddleware(a.GetSummaryReport))

	// Activity
	a.Get(appURLName+"activity", utils.ValidateMiddleware(a.GetAllActivity))
	a.Get(appURLName+"activity/{id}", utils.ValidateMiddleware(a.GetActivity))

	// Health Check
	a.Get(appURLName+"status", a.HealthCheck)
	a.Get(appURLName+"status/", a.HealthCheck)

}

// Users
func (a *App) Authenticate(w http.ResponseWriter, r *http.Request) {
	handler.Authenticate(a.DB, w, r)
}

func (a *App) GetAllUsers(w http.ResponseWriter, r *http.Request) {
	handler.GetAllUsers(a.DB, w, r)
}

func (a *App) GetUser(w http.ResponseWriter, r *http.Request) {
	handler.GetUser(a.DB, w, r)
}

func (a *App) CreateUser(w http.ResponseWriter, r *http.Request) {
	handler.CreateUser(a.DB, w, r)
}

func (a *App) UpdateUser(w http.ResponseWriter, r *http.Request) {
	handler.UpdateUser(a.DB, w, r)
}

func (a *App) ResetUserPwd(w http.ResponseWriter, r *http.Request) {
	handler.UpdateUser(a.DB, w, r)
}

func (a *App) GeneratePassword(w http.ResponseWriter, r *http.Request) {
	handler.GeneratePassword(a.DB, w, r)
}

// Clients
func (a *App) CreateClient(w http.ResponseWriter, r *http.Request) {
	handler.CreateClient(a.DB, w, r)
}

func (a *App) GetClient(w http.ResponseWriter, r *http.Request) {
	handler.GetClient(a.DB, w, r)
}

func (a *App) UpdateClient(w http.ResponseWriter, r *http.Request) {
	handler.UpdateClient(a.DB, w, r)
}

func (a *App) GetAllClients(w http.ResponseWriter, r *http.Request) {
	handler.GetAllClients(a.DB, w, r)
}

// Sales Representatives
func (a *App) CreateSalesRepresentative(w http.ResponseWriter, r *http.Request) {
	handler.CreateSalesRepresentative(a.DB, w, r)
}

func (a *App) GetSalesRepresentative(w http.ResponseWriter, r *http.Request) {
	handler.GetSalesRepresentative(a.DB, w, r)
}

func (a *App) UpdateSalesRepresentative(w http.ResponseWriter, r *http.Request) {
	handler.UpdateSalesRepresentative(a.DB, w, r)
}

func (a *App) GetAllSalesRepresentatives(w http.ResponseWriter, r *http.Request) {
	handler.GetAllSalesRepresentatives(a.DB, w, r)
}

// Collections
func (a *App) CreateCollection(w http.ResponseWriter, r *http.Request) {
	handler.CreateCollection(a.DB, w, r)
}

func (a *App) GetCollection(w http.ResponseWriter, r *http.Request) {
	handler.GetCollection(a.DB, w, r)
}

func (a *App) UpdateCollection(w http.ResponseWriter, r *http.Request) {
	handler.UpdateCollection(a.DB, w, r)
}

func (a *App) GetAllCollections(w http.ResponseWriter, r *http.Request) {
	handler.GetAllCollections(a.DB, w, r)
}

// Reports
func (a *App) GetSummaryReport(w http.ResponseWriter, r *http.Request) {
	handler.GetSummaryReport(a.DB, w, r)
}

// Activity
func (a *App) GetAllActivity(w http.ResponseWriter, r *http.Request) {
	handler.GetAllActivity(a.DB, w, r)
}

func (a *App) GetActivity(w http.ResponseWriter, r *http.Request) {
	handler.GetActivity(a.DB, w, r)
}

// HealthCheck - HealthCheck
func (a *App) HealthCheck(w http.ResponseWriter, r *http.Request) {
	handler.HealthCheck(a.Config, a.DB, w, r)
}

// Run the app on it's router
func (a *App) Run(host string) {

	headers := handlers.AllowedHeaders([]string{
		"X-Requested-With", "Content-Type", "Authorization",
		"Access-Control-Allow-Origin", "http://localhost:3000",
		"Access-Control-Allow-Origin", "*"})
	methods := handlers.AllowedMethods([]string{"GET", "POST", "PUT", "DELETE", "OPTIONS"})
	origins := handlers.AllowedHeaders([]string{"*"})

	log.Println("Application started on port >>> ", host)
	log.Fatal(http.ListenAndServe(host, handlers.CORS(headers, methods, origins)(a.Router)))

}
