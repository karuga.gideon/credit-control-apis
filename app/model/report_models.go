package model

// JSONResponse : Success response
// swagger:response jsonResponse
type JSONResponse struct {
	Status      string `json:"status"`
	Description string `json:"description"`
}

// CustomResponse struct
type CustomResponse struct {
	Status      string `json:"status"`
	Description string `json:"description"`
}

// ResponseStruct - Mostly repond with this in error responses
type ResponseStruct struct {
	Status  string
	Message string
}

// SummaryReport struct
type SummaryReport struct {
	CollectionsCount          float64 `json:"collections_count"`
	ClientsCount              float64 `json:"clients_count"`
	SalesRepresentativesCount float64 `json:"sales_representatives_count"`
	UsersCount                float64 `json:"users_count"`
}

// CollectionsReport Struct
type CollectionsReport struct {
	Collection          Collection          `json:"collection"`
	Client              Client              `json:"client"`
	SalesRepresentative SalesRepresentative `json:"sales_representative"`
}

// CollectionsApiReport struct
type CollectionsApiReport struct {
	CollectionsCount float64             `json:"collections_count"`
	Collections      []CollectionsReport `json:"collections"`
}

// ClientsApiReport struct
type ClientsApiReport struct {
	ClientsCount float64  `json:"clients_count"`
	Clients      []Client `json:"clients"`
}

// SalesRepsApiReport struct
type SalesRepsApiReport struct {
	SalesRepsCount       float64               `json:"sales_reps_count"`
	SalesRepresentatives []SalesRepresentative `json:"sales_reps"`
}

// UsersRepsApiReport struct
type UsersRepsApiReport struct {
	UsersCount float64 `json:"users_count"`
	Users      []User  `json:"users"`
}
