package model

import (
	"time"

	"github.com/jinzhu/gorm"
)

// User - User table / model
type User struct {
	ID              int       `json:"id"`
	Name            string    `json:"name"`
	Phone           string    `json:"phone"`
	Email           string    `json:"email" gorm:"unique;not null"`
	Username        string    `json:"username"`
	Password        string    `json:"password"`
	Token           string    `json:"token"`
	TokenExpiryTime time.Time `json:"token_expiry_time"`
	DateCreated     time.Time `json:"date_created"`
	Status          string    `json:"status"`
}

// Client - Client table / model
type Client struct {
	ID          int       `json:"id"`
	DateCreated time.Time `json:"date_created"`
	Name        string    `json:"name"`
	Phone       string    `json:"phone"`
	Email       string    `json:"email"`
	Location    string    `json:"location"`
	Status      int       `json:"status"` // 0 - Inactive , 1 - Pending activation, 2 - Approved, 3. - Deactivated
}

// Collection - Collection table / model
type Collection struct {
	ID                    int       `json:"id"`
	DateCreated           time.Time `json:"date_created"`
	ClientID              int       `json:"client_id"`
	InvoiceNumber         string    `json:"invoice_number"  gorm:"unique;not null"`
	InvoiceDate           time.Time `json:"invoice_date"`
	DueDate               time.Time `json:"due_date"`
	CreditPeriod          string    `json:"credit_period"`
	Amount                float64   `json:"amount"`
	AdvancePayment        float64   `json:"advance_payment"`
	Balance               float64   `json:"balance"`
	SalesRepresentativeID int       `json:"sales_representative_id"`
	SalesCommission       float64   `json:"sales_commission"` // 5% - Hard Coded
}

// Payment - Payment table / model
type Payment struct {
	ID            int       `json:"id"`
	DateCreated   time.Time `json:"date_created"`
	InvoiceNumber string    `json:"invoice_number"`
	Amount        float64   `json:"amount"`
}

// SalesRepresentative - SalesRepresentative table / model
type SalesRepresentative struct {
	ID          int       `json:"id"`
	DateCreated time.Time `json:"date_created"`
	Name        string    `json:"name"`
	Phone       string    `json:"phone" gorm:"unique;not null"`
	Email       string    `json:"email" gorm:"unique;not null"`
}

// Activity - Activity table / model
type Activity struct {
	ID           int       `json:"id"`
	DateCreated  time.Time `json:"date_created"`
	UserID       int       `json:"user_id"`
	ActivityType string    `json:"activity_type"`
	Description  string    `json:"description"`
}

// SysMsg - SysMsg table / model
type SysMsg struct {
	ID          int       `json:"id"`
	DateCreated time.Time `json:"date_created"`
	DateSent    time.Time `json:"date_sent"`
	MessageType string    `json:"message_type"`
	Recipient   string    `json:"recipient"`
	Subject     string    `json:"subject"`
	Message     string    `json:"message"`
	Status      int       `json:"status"`
}

// DBMigrate will create and migrate the tables, and then make the some relationships if necessary
func DBMigrate(db *gorm.DB) *gorm.DB {
	db.AutoMigrate(&User{}, &Client{}, &Collection{}, &Payment{}, &SalesRepresentative{}, &Activity{}, &SysMsg{})
	return db
}
