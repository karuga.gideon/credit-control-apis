package utils

import (
	"credit-control-kent/app/model"
	"crypto/aes"
	"crypto/cipher"
	cryptoRand "crypto/rand"
	"crypto/sha256"
	"encoding/base64"
	"errors"
	"fmt"
	"io"
	"log"
	"math/rand"
	"net/smtp"
	"strconv"
	"strings"
	"time"
)

// Base64DecodeString - Base64DecodeString
func Base64DecodeString(str string) string {
	data, err := base64.StdEncoding.DecodeString(str)
	if err != nil {
		fmt.Println("error:", err)
		return ("")
	}
	strData := fmt.Sprintf("%s", data)
	return strData
}

// GetErrorCode - Gets the error code  / message from MTN - Error Response
func GetErrorCode(errorString string) string {
	var errorCode = errorString
	errorStart := strings.LastIndex(errorCode, "errorcode")
	errorCode = errorCode[errorStart:len(errorCode)]
	errorEnd := strings.Index(errorCode, ">")
	errorCode = errorString[errorStart:(errorStart + errorEnd)]
	errorCode = strings.Replace(errorCode, "errorcode=", "", 1)
	errorCode = strings.Replace(errorCode, "/", "", 1)
	errorCode = strings.Replace(errorCode, "\"", "", 2)
	return errorCode
}

// GenerateRandomString - Random string generation for passwords
func GenerateRandomString(n int) string {
	rand.Seed(time.Now().UnixNano())
	var letterRunes = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")
	b := make([]rune, n)
	for i := range b {
		b[i] = letterRunes[rand.Intn(len(letterRunes))]
	}
	return string(b)
}

// DecryptString -- Takes two strings, cryptoText and keyString.
// cryptoText is the text to be decrypted and the keyString is the key to use for the decryption.
// The function will output the resulting plain text string with an error variable.
func DecryptString(cryptoText string, keyString string) (plainTextString string, err error) {
	// Format the keyString so that it's 32 bytes.
	newKeyString, err := hashTo32Bytes(keyString)
	// Encode the cryptoText to base 64.
	cipherText, _ := base64.URLEncoding.DecodeString(cryptoText)
	block, err := aes.NewCipher([]byte(newKeyString))
	if err != nil {
		panic(err)
	}
	if len(cipherText) < aes.BlockSize {
		panic("cipherText too short")
	}
	iv := cipherText[:aes.BlockSize]
	cipherText = cipherText[aes.BlockSize:]
	stream := cipher.NewCFBDecrypter(block, iv)
	stream.XORKeyStream(cipherText, cipherText)
	return string(cipherText), nil
}

// EncryptString - Takes two string, plainText and keyString.
// plainText is the text that needs to be encrypted by keyString.
// The function will output the resulting crypto text and an error variable.
func EncryptString(plainText string, keyString string) (cipherTextString string, err error) {
	// Format the keyString so that it's 32 bytes.
	newKeyString, err := hashTo32Bytes(keyString)
	if err != nil {
		return "", err
	}
	key := []byte(newKeyString)
	value := []byte(plainText)
	block, err := aes.NewCipher(key)
	if err != nil {
		panic(err)
	}
	cipherText := make([]byte, aes.BlockSize+len(value))
	iv := cipherText[:aes.BlockSize]
	if _, err = io.ReadFull(cryptoRand.Reader, iv); err != nil {
		return
	}
	cfb := cipher.NewCFBEncrypter(block, iv)
	cfb.XORKeyStream(cipherText[aes.BlockSize:], value)
	return base64.URLEncoding.EncodeToString(cipherText), nil
}

// As we cannot use a variable length key, we must cut the users key
// up to or down to 32 bytes. To do this the function takes a hash
// of the key and cuts it down to 32 bytes.
func hashTo32Bytes(input string) (output string, err error) {
	if len(input) == 0 {
		return "", errors.New("No input supplied")
	}
	hasher := sha256.New()
	hasher.Write([]byte(input))
	stringToSHA256 := base64.URLEncoding.EncodeToString(hasher.Sum(nil))
	// Cut the length down to 32 bytes and return.
	return stringToSHA256[:32], nil
}

// DelaySeconds - delay for a number of seconds
func DelaySeconds(n time.Duration) {
	time.Sleep(n * time.Second)
}

// SendEmail - Send email using golang / zoho - working Ok.
func SendEmail(message model.SysMsg) {
	log.Print("Sending email via zoho...")
	from := "info@bitmulla.com"
	pass := "Defjam2009#"
	to := message.Recipient
	mime := "MIME-version: 1.0;\nContent-Type: text/html; charset=\"UTF-8\";\n\n"
	subject := "Subject: " + message.Subject + "\n"
	log.Print(subject)
	msg := []byte(subject + mime + "<html><body><p>" + message.Message + "</p></body></html>")
	// smtp.SendMail(server, auth, from, to, msg)
	err := smtp.SendMail("smtp.zoho.com:587",
		smtp.PlainAuth("", from, pass, "smtp.zoho.com"),
		from, []string{to}, msg)

	if err != nil {
		log.Printf("smtp error: %s", err)
		return
	}
	log.Print("Email sent.")
}

// SendEmailGmail - Send email using golang
func SendEmailGmail(body string) {
	log.Print("Sending email...")
	from := "gidemn@gmail.com"
	pass := "Defjam2009#"
	to := "gidemn@gmail.com"

	msg := "From: " + from + "\n" +
		"To: " + to + "\n" +
		"Subject: Hello there\n\n" +
		body

	err := smtp.SendMail("smtp.gmail.com:587",
		smtp.PlainAuth("", from, pass, "smtp.gmail.com"),
		from, []string{to}, []byte(msg))

	if err != nil {
		log.Printf("smtp error: %s", err)
		return
	}
	log.Print("Email sent.")
}

// SendEmailZohoTest - SendEmail  Via Zoho
func SendEmailZohoTest(body string) {
	from := "info@bitmulla.com"
	auth := smtp.PlainAuth("", from, "Defjam2009#", "smtp.zoho.com")
	err := smtp.SendMail(
		"smtp.zoho.com:465",                 // server address
		auth,                                // authentication
		from,                                // sender's address
		[]string{"karuga.gideon@gmail.com"}, // recipients' address
		[]byte("Hello World!"),              // message body
	)
	if err != nil {
		log.Print(err)
	}
}

// RandomStringGenerator - Genererate a random string o n length
func RandomStringGenerator(n int) string {
	rand.Seed(time.Now().UnixNano())
	var letterRunes = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-TEST")
	b := make([]rune, n)
	for i := range b {
		b[i] = letterRunes[rand.Intn(len(letterRunes))]
	}
	return string(b)
}

// CheckError - check for err globally
func CheckError(err error, description string) {
	// raven.SetDSN("https://4721f61661b945cd97dc2eeea0230658:6aecf01aeaa6470b91b7b9e4d9ffd9da@sentry.io/243952-TEST")
	if err != nil {
		// panic(er) // CaptureErrorAndWait // CaptureError
		log.Println("Err ecountered on >>> ", description)
		log.Println("Encountered an Error >>> ", err.Error())
		// result := raven.CaptureError(err, nil)
		// log.Println("Go sentry >>> ", result)
		// result := raven.CaptureErrorAndWait(err, nil)
		// log.Println("Go sentry [2] >>> ", result)
	}
}

// ConvertStringToInt - ConvertStringToInt
func ConvertStringToInt(str string) int {
	j, err := strconv.Atoi(str)
	if err != nil {
		log.Println("Error converting string id to int >>> ", j)
		return 0
	}
	return j
}
