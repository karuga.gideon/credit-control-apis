package threads

import (
	"fmt"
	"save-pay/app/utils"
	"save-pay/config"

	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
)

// App has router and db instances
type App struct {
	Router *mux.Router
	DB     *gorm.DB
	Config *config.Config
}

// CheckPendingTransactions - Checks for pending transactions - cash in
// Whether the user confirmed or not
func CheckPendingTransactions(a *App) {
	log.Println("Checking for pending transactions to process...")
	utils.DelaySeconds(5)
}
